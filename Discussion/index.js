// JSON objects
// JSON stands for JavaScript Object Notation
// JSON is a data format used by JavaScript as well as other programming languages
// JSON Objects are NOT to be confused with JavaScript Objects
// Use of double quotation marks is requires in JSON Objects

// Syntax:
// {
//      "propertyA": "valueA"
//      "propertyB": "valueB"
//      
// }

// {"city": "Quezon City"}
// {"province": "Metro Manila"}
// {"country": "Philippines"}

// JavaScript Array of Objects:

let myArr = [
    {name: "Jino" },
    { name: "John" }
]

// Converting JS Data Into Stringified JSON
// Stringified JSON is a JavaScript Object converted into a string to be used by the receiving back-end application or  other functions of a JavaScript application.

let batchesArr = [
    {batchname: 'Batch 145'},
    {batchname: 'Batch 146'},
    {batchname: 'Batch 147'}
]

let stringifiedData = JSON.stringify(batchesArr)
// console.log(stringifiedData) 
//RSULT
// '[{"batchname":"Batch 145"},{"batchname":"Batch 146"},{"batchname":"Batch 147"}]'

// Converting Stringnified JSON into JavaScript Objects
let fixedData = JSON.parse(stringifiedData)
console.log(fixedData) 
//RESULT
//{batchname: 'Batch 145'}
//{batchname: 'Batch 146'}
//{batchname: 'Batch 147'}
